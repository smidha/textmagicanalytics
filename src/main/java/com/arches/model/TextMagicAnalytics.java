package com.arches.model;

public class TextMagicAnalytics
{
	private Long id;
	private Float replyRate;
	private String date;
	private Float deliveryRate;
	private Double costs;
	private Long messagesReceived;
	private Long messagesSentDelivered;
	private Long messagesSentAccepted;
	private Long messagesSentBuffered;
	private Long messagesSentFailed;
	private Long messagesSentRejected;
	private Long messagesSentParts;
	private String startDate;
	private String endDate;
	private String entryTZ;
	private String entryDate;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getEntryTZ() {
		return entryTZ;
	}
	public void setEntryTZ(String entryTZ) {
		this.entryTZ = entryTZ;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public Float getReplyRate() {
		return replyRate;
	}
	public void setReplyRate(Float replyRate) {
		this.replyRate = replyRate;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Float getDeliveryRate() {
		return deliveryRate;
	}
	public void setDeliveryRate(Float deliveryRate) {
		this.deliveryRate = deliveryRate;
	}
	public Double getCosts() {
		return costs;
	}
	public void setCosts(Double costs) {
		this.costs = costs;
	}
	public Long getMessagesReceived() {
		return messagesReceived;
	}
	public void setMessagesReceived(Long messagesReceived) {
		this.messagesReceived = messagesReceived;
	}
	public Long getMessagesSentDelivered() {
		return messagesSentDelivered;
	}
	public void setMessagesSentDelivered(Long messagesSentDelivered) {
		this.messagesSentDelivered = messagesSentDelivered;
	}
	public Long getMessagesSentAccepted() {
		return messagesSentAccepted;
	}
	public void setMessagesSentAccepted(Long messagesSentAccepted) {
		this.messagesSentAccepted = messagesSentAccepted;
	}
	public Long getMessagesSentBuffered() {
		return messagesSentBuffered;
	}
	public void setMessagesSentBuffered(Long messagesSentBuffered) {
		this.messagesSentBuffered = messagesSentBuffered;
	}
	public Long getMessagesSentFailed() {
		return messagesSentFailed;
	}
	public void setMessagesSentFailed(Long messagesSentFailed) {
		this.messagesSentFailed = messagesSentFailed;
	}
	public Long getMessagesSentRejected() {
		return messagesSentRejected;
	}
	public void setMessagesSentRejected(Long messagesSentrejected) {
		this.messagesSentRejected = messagesSentrejected;
	}
	public Long getMessagesSentParts() {
		return messagesSentParts;
	}
	public void setMessagesSentParts(Long messagesSentParts) {
		this.messagesSentParts = messagesSentParts;
	}
	
}
