package com.arches.model;

import java.math.BigDecimal;

public class Configuration
{
	private String username;
	private String password;
	private String paramBy;
	private String paramStartDate;
	private String paramEndDate;
	private String dbUsername;
	private String dbPassword;
	private String dbUrl;
	private String dbDriver;
	private boolean stopApp;
	private boolean scheduleQuartzJob;
	private String textMagicAnalyticsLogs;
	private Boolean dayByDayData;
	
	
	
	public Boolean getDayByDayData() {
		return dayByDayData;
	}
	public void setDayByDayData(Boolean dayByDayData) {
		this.dayByDayData = dayByDayData;
	}
	public String getTextMagicAnalyticsLogs() {
		return textMagicAnalyticsLogs;
	}
	public void setTextMagicAnalyticsLogs(String textMagicAnalyticsLogs) {
		this.textMagicAnalyticsLogs = textMagicAnalyticsLogs;
	}
	public boolean isStopApp() {
		return stopApp;
	}
	public void setStopApp(boolean stopApp) {
		this.stopApp = stopApp;
	}
	public boolean isScheduleQuartzJob() {
		return scheduleQuartzJob;
	}
	public void setScheduleQuartzJob(boolean scheduleQuartzJob) {
		this.scheduleQuartzJob = scheduleQuartzJob;
	}
	public String getDbDriver() {
		return dbDriver;
	}
	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	}
	public String getDbUsername() {
		return dbUsername;
	}
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getParamBy() {
		return paramBy;
	}
	public void setParamBy(String paramBy) {
		this.paramBy = paramBy;
	}
	public String getParamStartDate() 
	{
	
		return paramStartDate;
	}
	public void setParamStartDate(String paramStartDate) 
	{
		this.paramStartDate =paramStartDate;
	}
	public String getParamEndDate() {
		return paramEndDate;
	}
	public void setParamEndDate(String paramEndDate) 
	{
		
			this.paramEndDate = paramEndDate;
		
		
	}
	
	
}
