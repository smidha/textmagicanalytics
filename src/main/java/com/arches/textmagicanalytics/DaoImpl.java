package com.arches.textmagicanalytics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.arches.model.TextMagicAnalytics;

@Repository
@Transactional

public class DaoImpl  {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	
	public void saveTextMagicData(TextMagicAnalytics textMagicAnalytics)
	{
		System.out.println("saving TM data...");
		Serializable result=getSessionFactory().getCurrentSession().save(textMagicAnalytics);
		
		System.out.println("saved:"+result.toString());
	
	}	
	
	
}
