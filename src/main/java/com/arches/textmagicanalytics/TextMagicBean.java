package com.arches.textmagicanalytics;

import org.springframework.stereotype.Component;

import com.arches.model.ApplicationContextProvider;
import com.arches.textmagicanalytics.App;
import com.textmagic.sdk.RestException;

//
	public class TextMagicBean {
	 
	
		public void runTextMagicAnalytics() throws RestException {
	    	
	    	System.out.println("-----------------------Running TM--------------------");
	    	App app=(App)ApplicationContextProvider.getApplicationContext().getBean("app");
	    	app.main(null);
	        
	    }
}

