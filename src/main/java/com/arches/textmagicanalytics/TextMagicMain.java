package com.arches.textmagicanalytics;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TextMagicMain {

	public static final String CONFIG_PATH="application-config.xml";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new ClassPathXmlApplicationContext(CONFIG_PATH);
	}

}
