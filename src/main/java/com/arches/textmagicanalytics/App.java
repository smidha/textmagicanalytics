package com.arches.textmagicanalytics;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.ws.rs.core.MediaType;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.context.ApplicationContext;

import com.arches.model.ApplicationContextProvider;
import com.arches.model.Configuration;
import com.arches.model.TextMagicAnalytics;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.textmagic.sdk.RequestMethod;
import com.textmagic.sdk.RestClient;
import com.textmagic.sdk.RestException;
import com.textmagic.sdk.RestResponse;

/**
 * Hello world!
 *
 */
public class App 
{
	static Configuration configuration;
	static DaoImpl daoImpl;
	static String entryDate=null;
	static String entryTZ=null;
	static DateFormat dateFormat;
	static DateFormat tsFormat;
	static  DateFormat timeFormat;
	public static Logger textMagicLogger;
	private static FileHandler fileHandler;
   	  
	  
    public static void main( String[] args ) throws RestException
    {
    	  System.out.println("entered App main...");
      		configuration=(Configuration)ApplicationContextProvider.getApplicationContext().getBean("configuration");
      		daoImpl=(DaoImpl)ApplicationContextProvider.getApplicationContext().getBean("daoImpl");

      		textMagicLogger=Logger.getLogger("textMagicLogger");
      		
    try {
      	  	


  			fileHandler=new FileHandler(configuration.getTextMagicAnalyticsLogs()+new SimpleDateFormat("MM-dd-yyyy").format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime()).toString()+".txt",true);
	  		textMagicLogger.addHandler(fileHandler);
	  		SimpleFormatter simpleFormatter=new SimpleFormatter();
	  		fileHandler.setFormatter(simpleFormatter);
	  	   textMagicLogger.log(Level.INFO,"TextMagic Analytics logging for:"+Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());

    	  TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		  dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		  timeFormat = new SimpleDateFormat("hh:mm:ss Z");
		  tsFormat=new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
		  Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		  Date calTime=cal.getTime();
		  entryDate=dateFormat.format(calTime);
		  entryTZ=timeFormat.format(calTime);
		  
		  Boolean dayByDayData=configuration.getDayByDayData();
		  
        
    	
    	//get params from config for rest client
    	Map<String,String> paramsMap=getStartEndDates(cal);   	
    	List<NameValuePair> params = new ArrayList<NameValuePair>();
  	    //rest client
    	RestClient textMagicclient = new RestClient(configuration.getUsername(), configuration.getPassword());
   	    
    	//just get summary from start to end date
       if(dayByDayData==false && !paramsMap.get("startDateParam").equals(paramsMap.get("endDateParam")))
       {
    	   //populate params for rest client
    	  params.add(new BasicNameValuePair("by", configuration.getParamBy()));	  
    	  params.add(new BasicNameValuePair("start",paramsMap.get("startDateTSParam")));
	   	  params.add(new BasicNameValuePair("end",paramsMap.get("endDateTSParam")));
	   	  //get rest response
	      RestResponse response=textMagicclient.request("stats/messaging", RequestMethod.GET,params);
	      System.out.println("response:"+response.getJsonResponse());
	      //create textmagicanalytics obj for db storage
	      TextMagicAnalytics textMagicAnalyticsObj=(new Gson()).fromJson(((response.getJsonResponse()).replace("[","".trim())).replace("]","".trim()),TextMagicAnalytics.class);
	      textMagicAnalyticsObj.setEntryTZ(entryTZ);
	      textMagicAnalyticsObj.setEntryDate(entryDate);
	      textMagicAnalyticsObj.setStartDate(paramsMap.get("startDateParam"));
	      textMagicAnalyticsObj.setEndDate(paramsMap.get("endDateParam"));
	      
	     // daoImpl.saveTextMagicData(textMagicAnalyticsObj);
	      System.out.println("-----------------------------single row:Text magic data saved------------------------");
       }
       
       else
       {
    	   //temp vars store config proeprties values for start/end dates
    	   String tempStartDateTSParam=paramsMap.get("startDateTSParam");
    	   String tempEndDateTSParam=paramsMap.get("endDateTSParam");
    	   String tempStartDateParam=paramsMap.get("startDateParam");
    	   String tempEndDateParam=paramsMap.get("endDateParam");
    	   //var for while loop comparison.This param holds a value that does not get changed in loop
    	   String endDateParamForComparison=tempEndDateParam;
    	
    	   //lists for start end dates for day by day account
    	   List<String> startDatesList=new ArrayList<String>();
    	   List<String> endDatesList=new ArrayList<String>();
    	   List<String> startDatesTSList=new ArrayList<String>();
    	   List<String> endDatesTSList=new ArrayList<String>();
    	   //get timestamp for start and end date provided
    	   Long long_startDateTS=Long.parseLong(tempStartDateTSParam);
		   Long long_endDateTS=Long.parseLong(tempEndDateTSParam);
		   //var for while loop comparison. value updates in loop
		   String startDateForComparison=tempStartDateParam;
		   
		   
		  if(!(startDateForComparison.equals(endDateParamForComparison)))
		  {  
    	   while(!startDateForComparison.equals(endDateParamForComparison))
    	   {
    		   
    		  
    		   try
    		   {
				//end date after 23 hrs 59 min 59 sec of start date-this becomes 1 full day's log
				long_endDateTS=long_startDateTS+23*60*60+59*60+59;
				//add TS to lists		
				startDatesTSList.add(long_startDateTS.toString());
				endDatesTSList.add(long_endDateTS.toString());
				//add corresponding dates to lists
				startDatesList.add(dateFormat.format(new Date(long_startDateTS*1000)));
				endDatesList.add(dateFormat.format(new Date(long_endDateTS*1000)));
				//update comparison var to latest added value to start dates list
				startDateForComparison=startDatesList.get(startDatesList.size()-1);
				//increment start date TS by 1 second to move it to next date 
				long_startDateTS=long_endDateTS+1;
				
			   }
    		   catch (Exception e)
    		   {
				// TODO Auto-generated catch block
				e.printStackTrace();
				textMagicLogger.log(Level.SEVERE,e.getMessage(),e);
			   }
    		   
    	   }
		  }
		  else
		  {
			  startDatesList.add(tempStartDateParam);
			  startDatesTSList.add(tempStartDateTSParam);
			  endDatesList.add(tempEndDateParam);
			  endDatesTSList.add(tempEndDateTSParam);
		  }
    	   
    	   for(int i=0;i<startDatesList.size();i++)
    	   {
    		   params.add(new BasicNameValuePair("by", configuration.getParamBy()));
    		   params.add(new BasicNameValuePair("start",startDatesTSList.get(i)));
    		   	  params.add(new BasicNameValuePair("end",endDatesTSList.get(i)));
    		   	 RestResponse response=textMagicclient.request("stats/messaging", RequestMethod.GET,params);
    		      System.out.println("res:"+response.getJsonResponse());
    		      TextMagicAnalytics textMagicAnalyticsObj=(new Gson()).fromJson(((response.getJsonResponse()).replace("[","".trim())).replace("]","".trim()),TextMagicAnalytics.class);
    		      textMagicAnalyticsObj.setEntryTZ(entryTZ);
    		      textMagicAnalyticsObj.setEntryDate(entryDate);
    		      textMagicAnalyticsObj.setStartDate(startDatesList.get(i));
    		      textMagicAnalyticsObj.setEndDate(endDatesList.get(i));
    		      
    		      daoImpl.saveTextMagicData(textMagicAnalyticsObj);
    		      System.out.println("-----------------------------multirow:Text magic data saved------------------------");
    		      params.clear();
    	       
    	   }
    	   
       }
    }
		catch(Exception e)
		{
			e.printStackTrace();
			textMagicLogger.log(Level.SEVERE,e.getMessage(),e);
		}
    finally
	{
		for(Handler h:textMagicLogger.getHandlers())
		{
			System.out.println("closing log file...");
		    h.close();   //must call h.close or a .LCK file will remain.
		}
	}

       
    }


public static Map getStartEndDates(Calendar cal)
{
	Long secondsDiff=null;
	Map<String,String> map=new HashMap<String, String>();
	String startDateTSParam=null;
	String startDateParam=null;
	String endDateTSParam=null;
	String endDateParam=null;
	if(configuration.getParamStartDate().equals("0"))
	{
		cal.add(Calendar.DATE,-1);
		
		try 
		{
		secondsDiff=((((tsFormat.parse(dateFormat.format(cal.getTime())+" 00:00:00")).getTime())/1000)-(tsFormat.parse("01-01-1970 00:00:00")).getTime()/1000);
		startDateTSParam=secondsDiff.toString();
		map.put("startDateTSParam",startDateTSParam);
		startDateParam=dateFormat.format(cal.getTime());
		map.put("startDateParam",startDateParam);
		System.out.println("start date seconds diff:"+secondsDiff);
		} catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			textMagicLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		
	}
	else
	{
		try 
		{
			secondsDiff=((tsFormat.parse(configuration.getParamStartDate().toString()+" 00:00:00")).getTime()/1000)-((tsFormat.parse("01-01-1970 00:00:00").getTime()/1000));
			
		}
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		startDateTSParam=secondsDiff.toString();
		startDateParam=configuration.getParamStartDate().toString();
		map.put("startDateTSParam",startDateTSParam);
		map.put("startDateParam",startDateParam);
	}
	
	if(configuration.getParamEndDate().equals("0"))
	{
			try 
		{
			secondsDiff=((((tsFormat.parse(dateFormat.format(cal.getTime())+" 23:59:59")).getTime())/1000)-(tsFormat.parse("01-01-1970 00:00:00")).getTime()/1000);
		   endDateTSParam=secondsDiff.toString();
		   endDateParam=dateFormat.format(cal.getTime());
			map.put("endDateTSParam",endDateTSParam);
			map.put("endDateParam",endDateParam);
		   System.out.println("end date seconds diff:"+secondsDiff);
		} 
			catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}    		
	}	
		
	else
	{
		try {
			secondsDiff=((tsFormat.parse(configuration.getParamEndDate().toString()+" 23:59:59")).getTime()/1000)-((tsFormat.parse("01-01-1970 00:00:00").getTime())/1000);
			
		}
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			textMagicLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		endDateTSParam=secondsDiff.toString();
		endDateParam=configuration.getParamEndDate();
		map.put("endDateTSParam",endDateTSParam);
		map.put("endDateParam",endDateParam);
	}
	
	
		
	cal.add(Calendar.DATE,+1);
	
	
    return map;	
 }
}