package com.arches.textmagicanalytics;

import java.sql.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;

import org.quartz.InterruptableJob;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.parsing.ParseState.Entry;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.arches.model.ApplicationContextProvider;
import com.textmagic.sdk.RestException;

public class ScheduledJob extends QuartzJobBean 
{

	private TextMagicBean textMagicBean;
	private boolean scheduleQuartzJob;
	
	public void setScheduleQuartzJob(boolean scheduleJob) {
		this.scheduleQuartzJob = scheduleJob;
	}

	public void setTextMagicBean(TextMagicBean tmBean) {
		this.textMagicBean = tmBean;
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
			{
			System.out.println("----executing quartz job------");
				ResourceBundle.clearCache();
				ResourceBundle bundle=ResourceBundle.getBundle("config");
				boolean scheduleQuartzJobFlag=Boolean.parseBoolean(bundle.getString("scheduleQuartzJob"));
				boolean stopApp=Boolean.parseBoolean(bundle.getString("stopApp"));
				
				if(stopApp==true)
				{
					System.out.println("stopApp flag for text magic analytics set to true..exiting");
					System.exit(0);
				}
				
				if(scheduleQuartzJobFlag==true)
				{
					System.out.println("----------------------------Scheduled Job Calling Text Magic-----------------------------");
					try {
						textMagicBean.runTextMagicAnalytics();
						
					} catch (RestException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
					
				{
					System.out.println("scheduleQuartzJob flag set to false..");
					//unschedule job set to true.
					
					Trigger trigger=(Trigger)ApplicationContextProvider.getApplicationContext().getBean("cronTrigger");
					Scheduler scheduler=(Scheduler)ApplicationContextProvider.getApplicationContext().getBean("schedulerFactoryBean");
			
				try 
				{
					//unschedule 
					boolean unschedulingSuccessful=scheduler.unscheduleJob(trigger.getKey());

					System.out.println("unscheduled Quartz Job:"+unschedulingSuccessful);	
				} 
				catch (SchedulerException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Exception in unscheduling quartz job."+e);
				}
				
				}
			
		
			}
			
		
	
	}

		
}